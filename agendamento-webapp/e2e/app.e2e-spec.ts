import { AgendamentoWebappPage } from './app.po';

describe('agendamento-webapp App', () => {
  let page: AgendamentoWebappPage;

  beforeEach(() => {
    page = new AgendamentoWebappPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
