import { NgModule } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const appRoutes: Routes = [
    { path: 'scheduling', loadChildren: 'app/scheduling/scheduling.module#SchedulingModule' },
    { path: 'customer', loadChildren: 'app/customer/customer.module#CustomerModule' },
    { path: 'login', redirectTo: 'auth/login' },
    { path: 'register', redirectTo: 'auth/register' },
    { path: 'auth', loadChildren: 'app/auth/auth.module#AuthModule' },
   // { path: 'auth/register', loadChildren: 'app/auth/auth.module#AuthModule' },
    { path: 'receive', loadChildren: 'app/finance/finance.module#FinanceModule' },
    { path: 'tasks', loadChildren: 'app/tasks/tasks.module#TasksModule' },
    { path: '', redirectTo: 'scheduling', pathMatch: 'full'},
    { path: '**', redirectTo: 'auth' }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule],
    declarations: []
})
export class AppRountingModule {
}


