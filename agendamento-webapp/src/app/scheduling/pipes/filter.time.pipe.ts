import { Pipe, PipeTransform } from '@angular/core';
import { Horario } from 'app/scheduling/models/Horario';
import { TableCellBlock } from 'app/scheduling/models/TableCellsBlock';

@Pipe({
  name: 'filterTime'
})

export class FilterTimePipe implements PipeTransform {
  transform(items: Horario[], filterRow: number, filterCol: number, cellBlock : TableCellBlock[]): Horario[] {

    if (filterCol === 0) {
      return items[filterCol][filterRow];
    }

    for (const cell of cellBlock.filter(item => item.Col === filterCol)){
      if ((cell.CellBlock - 1) >= filterRow) {
          return [];
      }
    }

    const result = items.filter(item => item.Row === filterRow);
    if (result.length <=  0) {
      return this.getEmptyArray(filterRow);
    }

    if ((<Horario>result[0]).RowCount > 1) {
      const tb = new TableCellBlock();
      tb.CellBlock = ((<Horario>result[0]).Row + (<Horario>result[0]).RowCount);
      tb.Col = filterCol;
      cellBlock.push(tb);
    }

    return result;
   }

   getEmptyArray(filterRow: number): Horario[] {
      const horario = new Horario();
      horario.Row = filterRow;
      horario.RowCount = 1;

      const lst = new Array();
      lst.push(horario);
      return lst;
   }
}
