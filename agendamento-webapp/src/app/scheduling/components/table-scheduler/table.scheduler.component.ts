import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { AgendaProfissional } from 'app/scheduling/models/AgendaProfissional';
import { Profissional } from 'app/scheduling/models/Profissional';
import { Horario } from 'app/scheduling/models/Horario';
import { TableCellBlock } from 'app/scheduling/models/TableCellsBlock';
import { HORARIOS_DEFAULT } from 'app/scheduling/models/Data';
import { DataService } from 'app/core';


@Component({
  selector: 'app-table-scheduler',
  templateUrl: './table.scheduler.component.html',
  styleUrls: ['./table.scheduler.component.css']
})
export class TableSchedulerComponent implements OnInit {

  characters: string[];
  agendaProfissionais: Observable<any>;
  horariosDefault: any[];
  columns: Profissional[];
  qtdHorarios: number[];
  cellsBlock: TableCellBlock[];
  intervaloParaAgendamento: number;

  constructor(private agendaDataService: DataService) { }

  ngOnInit() {

    this.intervaloParaAgendamento = 15;

    this.horariosDefault = this.getHorariosDeAgendamento();
    this.agendaProfissionais = this.agendaDataService.getAgendaProfissionais();
    this.agendaProfissionais.subscribe(val => this.generateColumns(val));
  }

  generateColumns(any) {
    //console.log('gerando');
    this.columns = new Array();
    this.cellsBlock = new Array();
    this.qtdHorarios = new Array();

    let defaultTimes = (<AgendaProfissional[]>this.horariosDefault)[0];
    for (let child of defaultTimes.Profissionais) {
      this.columns.push(child);
      if (this.qtdHorarios.length == 0) {
        this.qtdHorarios = new Array(child.Horarios.length);
      }
    }

    for (let child of (<AgendaProfissional>any).Profissionais) {
      this.columns.push(child);
    }
  }
  
  getHorariosDeAgendamento(): any[]{
    return HORARIOS_DEFAULT;
  }

  getClass(horario: Horario): string {
    let cssClass = "selectable center aligned";

    if (horario.Agendamento != undefined) {
      if(horario.Agendamento.Situacao == 1)
      {
        cssClass += " confirmado";
      }
      else if(horario.Agendamento.Situacao == 2)
      {
        cssClass += " em_atendimento";
      }
      else if(horario.Agendamento.Situacao == 3)
      {
        cssClass += " agendado";
      }
      else if(horario.Agendamento.Situacao == 4)
      {
        cssClass += " finalizado";
      }
      
    }

    return cssClass;
  }
}
