import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { SchedulingComponent } from './pages/scheduling/scheduling.component';
import { SchedulingRegisterComponent } from 'app/scheduling/pages/scheduling-register/scheduling-register.component';

import { FilterTimePipe } from 'app/scheduling/pipes/filter.time.pipe';
import { TableSchedulerComponent } from 'app/scheduling/components/table-scheduler/table.scheduler.component';
import { SchedulingRountingModule } from './scheduling.routing';


@NgModule({
    imports: [
        SharedModule,
        SchedulingRountingModule
    ],
    declarations: [
        FilterTimePipe,
        SchedulingRegisterComponent,
        TableSchedulerComponent,
        SchedulingComponent
    ],
    exports: [],
    providers: [],
    entryComponents:[SchedulingRegisterComponent]
})
export class SchedulingModule { }
