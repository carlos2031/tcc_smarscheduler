export const HORARIOS_DEFAULT: any[] =
[{
  "Profissionais": [{
    "Codigo": "1",
    "Nome": "horario",
    "Profissao": "",
    "ImagemUrl": "",
        "Horarios": [{
            "Row": 0,
            "RowCount":"08:00"
          },
          {
            "Row": 1,
            "RowCount":"08:15"
          },
          {
            "Row": 2,
            "RowCount":"08:30"
          },
          {
            "Row": 3,
            "RowCount":"08:45"
          },
          {
            "Row": 4,
            "RowCount":"09:00"
          },
          {
            "Row": 5,
            "RowCount":"09:15"
          },
          {
            "Row": 6,
            "RowCount":"09:30"
          },
          {
            "Row": 7,
            "RowCount":"09:45"
          },
          {
            "Row": 8,
            "RowCount":"10:00"
          },
          {
            "Row": 9,
            "RowCount":"10:15"
          },
          {
            "Row": 10,
            "RowCount":"10:30"
          },
          {
            "Row": 11,
            "RowCount":"10:45"
          },
          {
            "Row": 12,
            "RowCount":"11:00"
          },
          {
            "Row": 13,
            "RowCount":"11:15"
          },
          {
            "Row": 14,
            "RowCount":"11:30"
          },
          {
            "Row": 15,
            "RowCount":"11:45"
          }
          ,
          {
            "Row": 16,
            "RowCount":"12:00"
          },
          {
            "Row": 18,
            "RowCount":"12:15"
          },
          {
            "Row": 19,
            "RowCount":"12:30"
          },
          {
            "Row": 20,
            "RowCount":"12:45"
          },
          {
            "Row": 21,
            "RowCount":"13:00"
          },
          {
            "Row": 22,
            "RowCount":"13:15"
          },
          {
            "Row": 23,
            "RowCount":"13:30"
          },
          {
            "Row": 24,
            "RowCount":"13:45"
          },
          {
            "Row": 25,
            "RowCount":"14:00"
          },
          {
            "Row": 26,
            "RowCount":"14:15"
          },
          {
            "Row": 27,
            "RowCount":"14:30"
          },
          {
            "Row": 28,
            "RowCount":"14:45"
          },
          {
            "Row": 29,
            "RowCount":"15:00"
          },
          {
            "Row": 30,
            "RowCount":"15:15"
          },
          {
            "Row": 31,
            "RowCount":"15:30"
          },
          {
            "Row": 32,
            "RowCount":"15:45"
          }
          ,
          {
            "Row": 33,
            "RowCount":"16:00"
          }
          ,
          {
            "Row": 34,
            "RowCount":"16:15"
          }
          ,
          {
            "Row": 35,
            "RowCount":"16:30"
          }
    ]
  }]
}
]; 