import { ComponentModalConfig, ModalSize } from 'ng2-semantic-ui';
import { SchedulingRegisterComponent } from 'app/scheduling/pages/scheduling-register/scheduling-register.component';

export class ConfirmModal extends ComponentModalConfig<IConfirmModalContext, void, void> {
    constructor(title:string, question:string, size = ModalSize.Small, objeto: any = null) {
        super(SchedulingRegisterComponent, { title, question, objeto });

        this.isClosable = true;
        this.transitionDuration = 200;
        this.size = size;
    }
}


