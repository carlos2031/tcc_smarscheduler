import { AgendamentoItem } from 'app/scheduling/models/AgendamentoItem';

export class Agendamento
{
    public EntidadeId : string;
    public ServicoId: string;
    public SituacaoId: string;
    public TipoDoAgendamento: string;
    public TipoDeNotificacao: string;
    public Items: AgendamentoItem[] = [];
    
}

