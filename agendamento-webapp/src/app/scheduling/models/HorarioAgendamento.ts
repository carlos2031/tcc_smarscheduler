export class HorarioAgendamento
{
    public Guid: string;
    public HInicial: string;
    public HFinal: string;
    public Nome: string;
    public Situacao: number;
}