export class AgendamentoItem
{
    public ServicoId : string;
    public Servico: string;
    public ProfissionalId : string;
    public Profissional: string;
    public Data : Date;
    public Horario : number;
    public Duracao : number;
}