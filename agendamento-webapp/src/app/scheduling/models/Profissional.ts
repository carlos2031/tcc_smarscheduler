import { Horario } from "app/scheduling/models/Horario";

export class Profissional
{
    public Nome: string;
    public Codigo: number;
    public Profissao: string;
    public ImagemUrl: string;
    public Horarios: Horario[];
}