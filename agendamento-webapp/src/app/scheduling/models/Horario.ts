import { HorarioAgendamento } from "app/scheduling/models/HorarioAgendamento";

export class Horario
{
    public Row: number;
    public RowCount: number;
    public ImagemUrl: string;
    public Agendamento: HorarioAgendamento;
}