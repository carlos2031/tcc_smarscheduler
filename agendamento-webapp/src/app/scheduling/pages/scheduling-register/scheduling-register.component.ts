
import { ReactiveFormsModule, FormsModule, FormGroup, FormControl, NgForm } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { SuiModal } from 'ng2-semantic-ui';
import { DataService } from 'app/core';
import { Agendamento } from 'app/scheduling/models/Agendamento';
import { AgendamentoItem } from 'app/scheduling/models/AgendamentoItem';

@Component({
  selector: 'app-scheduling-register',
  templateUrl: './scheduling-register.component.html',
  styleUrls: ['./scheduling-register.component.css']
})
export class SchedulingRegisterComponent implements OnInit {
  public clientsOptions: IOption[] = [];
  public clientsSelected: IOption;
  
  public servicosOptions: IOption[] = [];
  public servicosSelected: IOption;
  
  public situacoesDeAgendamento: IOption[] = [];
  public situacoesDeAgendamentoSelected: IOption;
  public tiposDeAgendamento: IOption[] = [];
  public tiposDeAgendamentoSelected: IOption;
  public tiposDeNotificacao: IOption[] = [];
  public tiposDeNotificacaoSelected: IOption;
  public temposDeDuracaoPadrao: IOption[] = [];
  public temposDeDuracaoPadraoSelected: IOption;
  public horariosDisponiveis: IOption[] = [];
  public horarioDisponivelSelected: IOption;
  public profissionais: IOption[] = [];
  public profissionaisSelected: IOption;
  public datePickerMode: string;
  public datePicker: Date;
  public datePickerLocal: string;

  public agendamento: Agendamento;
  public userForm: FormGroup;
  
  constructor(public modal:SuiModal<IConfirmModalContext, void, void>,
              public dataService: DataService) { }

  ngOnInit() {
    this.loadSituacoesDeAgendamento();
    this.loadTiposDeAgendamento();
    this.loadTiposDeNotificacao();
    this.loadDuracoesPadrao();
    this.loadProfissionais();
    this.loadDateConfig();
    this.agendamento = new Agendamento();

    this.userForm = new FormGroup({
        email: new FormControl()
    });
  }

  public optionsSearch = (query: string) => {
    this.dataService.getItunesMusic(query)
                         .subscribe((data: IOption[]) => this.clientsOptions = data,
                                    error => console.log(error));

    return new Promise(resolve => {
      if (query == "") {
        resolve(this.clientsOptions);
      }

      const results = this.clientsOptions
                      .filter(o => o.title.toLowerCase().indexOf(query.toLowerCase()) >= 0);
      setTimeout(() => resolve(results), 300);
    });
  }

  public optionsSearchServices = (query: string) => {
    this.dataService.getServicos(query)
                    .subscribe((data: IOption[]) => this.servicosOptions = data,
                                error => console.log(error));

    return new Promise(resolve => {
      if (query == "") {
        resolve(this.servicosOptions);
      }

      const results = this.servicosOptions
                      .filter(o => o.title.toLowerCase().indexOf(query.toLowerCase()) >= 0);
      setTimeout(() => resolve(results), 300);
    });
  }

  loadSituacoesDeAgendamento()
  {
    this.dataService.getAgendamentoSituacoes()
                    .subscribe((dsResult: IOption[]) => this.situacoesDeAgendamento = dsResult, 
                               error => console.log(error));
  }

  loadTiposDeAgendamento()
  {
    this.dataService.getAgendamentoTiposDeCadastro()
                    .subscribe((dsResult: IOption[]) => this.tiposDeAgendamento = dsResult,
                               error => console.log(error));
  }

  loadTiposDeNotificacao()
  {
    this.dataService.getAgendamentoTiposDeNotificacao()
                    .subscribe((dsResult: IOption[]) => this.tiposDeNotificacao = dsResult,
                               error => console.log(error));
  }

  loadDuracoesPadrao()
  {
    this.dataService.getServicoDuracoesPadrao()
                    .subscribe((dsResult: IOption[]) => this.temposDeDuracaoPadrao = dsResult,
                               error => console.log(error));
  }

  loadProfissionais()
  {
    this.dataService.getProfissionais()
                    .subscribe((dsResult: IOption[]) => this.profissionais = dsResult,
                               error => console.log(error));
  }


  loadDatasDisponiveis(opcaoSelecionada: IOption)
  {
    this.profissionaisSelected = opcaoSelecionada;
    this.horariosDisponiveis = [];
    this.datePicker = new Date();
  }

  loadHorariosDisponiveis(opcaoSelecionada: Date)
  {
    this.datePicker = opcaoSelecionada;
    if(this.profissionaisSelected != undefined)
    {
    this.dataService.getHorariosPorProfissionalEData(this.profissionaisSelected.id,this.datePicker)
                    .subscribe((dsResult: IOption[]) => this.horariosDisponiveis = dsResult,
                               error => console.log(error));
    }
  }

  loadDateConfig()
  {
    this.datePickerMode = "date";
    this.datePickerLocal = "bottom";
    this.datePicker = new Date();
  }

  clientSelected(opcao: IOption)
  {
    console.log(opcao);
  }
  
  onFormSubmit(userForm: NgForm) {
    console.log(userForm.value);
  }

  imprime(valor:any){
    console.log(valor);
  }

  AddItem()
  {
    let item = new AgendamentoItem();
    item.ProfissionalId = this.profissionaisSelected.id;
    item.Profissional = this.profissionaisSelected.title;
    item.ServicoId = this.servicosSelected.id;
    item.Servico = this.servicosSelected.title;
    item.Horario = Number(this.horarioDisponivelSelected.id);
    item.Data = this.datePicker;
    item.Duracao = Number(this.temposDeDuracaoPadraoSelected.id);
    this.agendamento.Items.push(item);

    console.log(this.agendamento);
  }


  salvarAgendamento()
  {
      //alert(JSON.stringify(this.agendamento));
      this.modal.approve(undefined);
  }
}
