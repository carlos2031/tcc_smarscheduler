import { Component, OnInit } from '@angular/core';
import {SuiDatepickerModule} from 'ng2-semantic-ui';
import { ConfirmModal } from 'app/scheduling/models/ConfirmModal';
import {SuiModalService} from 'ng2-semantic-ui';
import { DataService } from 'app/core';

@Component({
  selector: 'app-scheduling',
  templateUrl: './scheduling.component.html',
  styleUrls: ['./scheduling.component.css']
})
export class SchedulingComponent implements OnInit {
  public companyOptions: IOption[] = [];
  public companySelectedOption: IOption;
  public datePickerMode: string;
  public datePickerDay: string;
  public datePickerDescription: string;
  public segmentDimmed: boolean;
  public datePicker: Date;
  public datePickerLocal: string;
  
  constructor(public modalService:SuiModalService,
              private dsEmpresa: DataService) { }

  ngOnInit() {
    this.loadCompany();
    this.loadSchedulingConfig();
    this.showDateDescription();
  }

  async datepickerClick()
  {
    this.showDateDescription();
    this.segmentDimmed = true;
    await this.delay(300);
    this.segmentDimmed = false;
  }

  showDateDescription() {
    let month = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];
    let week  = ['Domingo', 'Segunda-feira', 'Terça-feira', 'Quarta-feira', 'Quinta-feira', 'Sexta-feira', 'Sábado'];

    this.datePickerDay = week[this.datePicker.getDay()];
    this.datePickerDescription = this.datePicker.getDate() + ' de ' + month[this.datePicker.getMonth()] +' de '+ this.datePicker.getFullYear();
  } 

  addDay()
  {
    this.datePicker.setDate(this.datePicker.getDate() + 1);
    this.datepickerClick();
  }

  removeDay()
  {
    this.datePicker.setDate(this.datePicker.getDate() - 1);
    this.datepickerClick();
  }

  delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  newScheduling()
  {
    this.modalService
    .open(new ConfirmModal("Are you sure?", "Are you sure about accepting this?", "small"))
    //.onApprove(() => alert("User has accepted."))
    //.onDeny(() => alert("User has denied."))
    ;
  }

  loadSchedulingConfig()
  {
    this.datePickerMode = "date";
    this.datePickerLocal = "bottom";
    this.datePicker = new Date();
  }

  loadCompany()
  {
    this.dsEmpresa.getEmpresas().subscribe((dsResult: IOption[]) => this.companyOptions = dsResult, 
                                            error => console.log(error),
                                            () => this.companySelectedOption = this.companyOptions[0]);
     
  }
}
