import { NgModule, Component } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SchedulingComponent } from 'app/scheduling/pages/scheduling/scheduling.component';

const appRoutes: Routes = [
    { path: '', component: SchedulingComponent },
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule],
    declarations: []
})
export class SchedulingRountingModule {

}
