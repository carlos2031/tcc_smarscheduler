export const TiposDeNotificacao: IOption[] = [
    {id:"dfe0eea7-0408-428e-8f00-27564f926e7d", title: "SMS"},
    {id:"1d784fb1-9cf8-467b-b946-acae8c6733d3", title: "E-mail"},
    {id:"f9578f89-1541-4b32-93c9-3316e4b02e2f", title: "SMS/E-mail"}
];

export const TiposDeAgendamento: IOption[] = [
    {id:"52a880f2-6f52-4fd6-aaa0-254465e10b31", title: "Serviço"},
    {id:"3e8dc663-ceb0-428d-84da-bbfb5cee2b84", title: "Vistoria"},
    {id:"eaf927f0-9912-45d9-a68a-6a8a43aa4c6a", title: "Manutenção"}
];

export const SituacoesDeAgendamento: IOption[] = [
    {id:"ac47d228-5c53-41f8-8935-d9090dbf8860", title: "Agendado"},
    {id:"4db5c277-c6fb-4ae9-86b7-a23cb93c31d6", title: "Confirmado"},
    {id:"6341ea7c-5572-4313-9c16-fa9da458024a", title: "Cancelado"},
    {id:"68042d78-1eb3-4af7-8c35-5037af1231a0", title: "Em atendimento"},
    {id:"24fe9811-a6a3-4f96-be01-541d843a06b4", title: "Finalizado"}
];

export const DuracoesPadraoDeServico: IOption[] = [
    {id:"15", title: "00:15"},
    {id:"30", title: "00:30"},
    {id:"45", title: "00:45"},
    {id:"100", title: "01:00"},
    {id:"115", title: "01:15"},
    {id:"130", title: "01:30"},
    {id:"145", title: "01:45"},
    {id:"200", title: "02:00"},
    {id:"215", title: "02:15"},
    {id:"230", title: "02:30"},
    {id:"245", title: "02:45"},
    {id:"300", title: "03:00"}
];

