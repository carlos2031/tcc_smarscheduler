import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRountingModule } from './app.rounting.module';
import { AppComponent } from './app.component';

// Modules
//import { CustomerModule } from './customer/customer.module';
//import { SchedulingModule } from 'app/scheduling/scheduling.module';
import { SharedModule } from 'app/shared/shared.module';
import { CoreModule } from './core/core.module';
import { DataService } from 'app/core';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    ReactiveFormsModule,
    //CustomerModule,
    //SchedulingModule,
    CoreModule,
    SharedModule,
    AppRountingModule
  ],
  providers: [DataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
