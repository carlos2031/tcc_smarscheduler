/**
 * This barrel file provides the export for the lazy loaded AboutComponent.
 */
export * from './pipes/time.pipe';
export * from './pipes/trim.pipe';
export * from './pipes/capitalize.pipe';
export * from './ui/common-interface';
export * from './validators/custom.validators';
