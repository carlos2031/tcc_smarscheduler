import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'time' })
export class TimePipe implements PipeTransform{
  transform(text: string){
      let result = this.formatZeros(text,4);
      return result.substring(0, 2).toUpperCase() + ":" + result.substring(2, result.length);
  }

  formatZeros(input, n) {
		if(input === undefined)
			input = ""
		if(input.length >= n)
			return input
		var zeros = "0".repeat(n);
		return (zeros + input).slice(-1 * n)
	};
}