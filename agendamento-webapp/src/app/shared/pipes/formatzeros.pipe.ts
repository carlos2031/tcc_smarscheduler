import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'formatzeros'
})

export class FormatZerosPipe implements PipeTransform {
    transform(input: any, n: number): string {

        if (input === undefined) {
            return '';
        }

        if (input.length >= n) {
            return input;
        }

        const zeros = '0'.repeat(n);
        return (zeros + input).slice(-1 * n)
    }
}
