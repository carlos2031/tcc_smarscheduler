import { Injectable } from '@angular/core';

@Injectable()
export class CommonInterface {

    lockLoading(element) {
        document.getElementById(element).classList.add('is-loading');
        document.getElementById(element).setAttribute('disabled','disabled');
        document.getElementById(element).firstElementChild.setAttribute('disabled','disabled');
    }

    unlockLoading(element) {
        document.getElementById(element).classList.remove('is-loading');
        document.getElementById(element).removeAttribute('disabled');
        document.getElementById(element).firstElementChild.removeAttribute('disabled');
    }
}
