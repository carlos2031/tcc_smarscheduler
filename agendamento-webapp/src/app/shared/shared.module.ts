import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { SuiModule } from 'ng2-semantic-ui';

import { CustomValidator } from 'app/shared/validators/custom.validators';
import { CommonInterface } from 'app/shared/ui/common-interface';

import { CapitalizePipe } from 'app/shared/pipes/capitalize.pipe';
import { TrimPipe } from './pipes/trim.pipe';
import { TimePipe } from './pipes/time.pipe';
import { CurrencyFormatPipe } from './pipes/currency.pipe';
import { FormatZerosPipe } from './pipes/formatzeros.pipe';
//import { AppRountingModule } from 'app/app.rounting.module';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SuiModule,
        //AppRountingModule
    ],
    declarations: [
        TrimPipe,
        CapitalizePipe,
        TimePipe,
        CurrencyFormatPipe,
        FormatZerosPipe
    ],
    exports: [
        CommonModule,
        FormsModule,
        SuiModule,
        //AppRountingModule,
        TrimPipe,
        CapitalizePipe,
        CurrencyFormatPipe,
        TimePipe,
        FormatZerosPipe,
        ReactiveFormsModule
    ],
    providers: [
        CustomValidator,
        CommonInterface
    ],
})
export class SharedModule { }
