import { Injectable } from '@angular/core';
import { FormControl } from '@angular/forms';

@Injectable()
export class CustomValidator {
    SelectValidator(control: FormControl) {
        var value: number = control.value.toString();

        if (value == 0) {
            return {
                "Selecione uma opção.": true
            }
        }
    }

    EmailValidator(control: FormControl) {
        var email_valid_expression = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/

        if (!email_valid_expression.test(control.value)) {
            return { "Email Inválido": true }
        }
        return null;
    }
}