import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router'
import { DataService } from 'app/core';
import { CustomValidator } from 'app/shared';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  providers: [DataService]
})
export class RegisterComponent implements OnInit {
  public form: FormGroup;
  public errors: any[] = [];

  constructor(private fb: FormBuilder,
              private dataService: DataService,
              private router: Router,
              private customValidator: CustomValidator) {

    this.form = this.fb.group({
      name: ['', Validators.compose([
        Validators.maxLength(40),
        Validators.minLength(6),
        Validators.required
      ])],
      lastname: ['', Validators.compose([
        Validators.maxLength(20),
        Validators.minLength(3),
        Validators.required
      ])],
      email: ['', Validators.compose([
        Validators.email,
        Validators.minLength(6),
        Validators.required,
        customValidator.EmailValidator
      ])],
      birthday: ['', Validators.compose([
        Validators.minLength(6),
        Validators.required
      ])],
      username: ['', Validators.compose([
        Validators.maxLength(40),
        Validators.minLength(6),
        Validators.required
      ])],
      password: ['', Validators.compose([
        Validators.maxLength(20),
        Validators.minLength(3),
        Validators.required
      ])],
      passwordconfirm: ['', Validators.compose([
        Validators.maxLength(20),
        Validators.minLength(3),
        Validators.required
      ])],
    });
  }

  ngOnInit() {
  }


  register() {
    this.dataService
      .register(this.form.value)
      .subscribe(result => {
        //alert("Cadastro realizado com sucesso!");
        this.router.navigateByUrl('/login')
      }, error => {
        this.errors = JSON.parse(error._body).errors;
      });
  }

}
