import { Component, OnInit } from '@angular/core';
import { Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { DataService } from 'app/core';
import { CommonInterface } from 'app/shared';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  public form: FormGroup;
  public errors: any[] = [];

  constructor(private fb: FormBuilder, private ui: CommonInterface, 
              private dataService: DataService, private router: Router) {
    this.form = this.fb.group({
      email: ['', Validators.compose([
        //Validators.email,
        Validators.minLength(6),
        Validators.required,
        //CustomValidator.EmailValidator
      ])],
      password: ['', Validators.compose([
        Validators.maxLength(20),
        Validators.minLength(3),
        Validators.required
      ])],
    });
  }

  ngOnInit() {
  }

  checkEmail() {
    //this.ui.lock('emailControl');

    setTimeout(() => {
      //this.ui.unlock('emailControl');
      console.log(this.form.controls.email.value);

    }, 1000);
  }

  authenticate() {
    //this.dataService
      //.authenticate(this.form.value)
      //.subscribe(result => {
        //localStorage.setItem('alearisolutions.ags.token', result.token);
        //localStorage.setItem('alearisolutions.ags.user', JSON.stringify(result.user));
        //console.log('ok'+ result.token);
        this.router.navigateByUrl('/scheduling')
      //}, error => {
       // this.errors = JSON.parse(error._body).errors;
       // console.log( this.errors);
      //});
  }
}
