import { NgModule } from '@angular/core';
import { AuthRoutingModule } from './auth.routing';
import { RegisterComponent } from './pages/register/register.component';
import { SharedModule } from 'app/shared/shared.module';
import { LoginComponent } from './pages/login/login.component';

@NgModule({
    imports: [
        SharedModule,
        AuthRoutingModule
    ],
    exports: [
        LoginComponent,
        RegisterComponent
    ],
    declarations: [
        LoginComponent,
        RegisterComponent,
    ],
    providers: [],
})
export class AuthModule { }
