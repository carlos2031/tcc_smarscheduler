import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'status_receipt'
})

export class StatusRecebimentoPipe implements PipeTransform {
    transform(value: string): string {
        let elementCss: string;

        switch (value.toLocaleLowerCase()) {
            case 'em aberto':
                elementCss = 'ui orange label';
                break;
            case 'pago':
                elementCss = 'ui green label customlabel';
                break;
            case 'cancelado':
                elementCss = 'ui red label';
                break;
            default:
                elementCss = 'ui gray label';
                break;
        }

        return `<a class='${elementCss}'>${value}</a>`;
    }
}
