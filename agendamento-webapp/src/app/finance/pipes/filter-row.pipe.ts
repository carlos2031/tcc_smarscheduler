import { Pipe, PipeTransform, Injectable } from '@angular/core';
@Pipe({
    name: 'filterRow'
})
@Injectable()
export class FilterRowPipe implements PipeTransform {
    transform(items: any[], field: string, value: string): any[] {

        if (!items) {
            return [];
        }
        if (!field || !value) {
            return items;
        }
        let result = items.filter(singleItem => singleItem[field].toLowerCase().includes(value.toLowerCase()));
        if (result.length <= 0) {
            result = items.filter(singleItem => singleItem['Código'].includes(value));
        }
        if (result.length <= 0) {
            result = items.filter(singleItem => singleItem['Cliente'].includes(value));
         }
        // if (result.length <= 0) {
        //     result = items.filter(singleItem => singleItem['Valor'].includes(value));
        // }

        return result;
    }
}