import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BillsToReceiveComponent } from './pages/bills-to-receive/bills-to-receive.component';

 const routes: Routes = [
   { path: '', component: BillsToReceiveComponent },
 ];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FinanceRoutingModule { }

export const routedComponents = [];
