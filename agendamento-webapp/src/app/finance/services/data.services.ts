import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { Recebimento } from 'app/finance/models/Recebimento';

@Injectable()
export class DataService {
    readonly UrlApi = 'http://localhost:3000';
    readonly UrlApiRecebimento = `${this.UrlApi}/recebimento`;

    constructor(private http: Http) { }


    getRecebimentos(situacao: string): Observable<Recebimento[]> {

        let UrlRecebimento = this.UrlApiRecebimento;
        if(situacao != '' && situacao != 'Todos')
        {
            UrlRecebimento += `?Situacao=${situacao}`
        }
        console.log(UrlRecebimento);
        return this.http.get(UrlRecebimento)
            .map((response: Response) => {
                let result = response.json().map(item => <Recebimento>item);
                //console.log(result);
                return result;
            }
        );
    } 
    
}