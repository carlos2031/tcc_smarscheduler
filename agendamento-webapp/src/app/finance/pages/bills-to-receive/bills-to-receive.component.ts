import { getTestBed } from '@angular/core/testing';
import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { Recebimento } from 'app/finance/models/Recebimento';
import { DataService } from 'app/finance/services/data.services';

@Component({
  selector: 'app-bills-to-receive',
  templateUrl: './bills-to-receive.component.html',
  styleUrls: ['./bills-to-receive.component.css'],
  providers: [DataService]
})
export class BillsToReceiveComponent implements OnInit {

  public recebimentos: Recebimento[];
  public searchString: string;
  public situacaoSelectedOption: IOption;
  public situacoes: IOption[] = [{ id: '0', title: 'Todos' }, { id: '1', title: 'Em aberto' },
                                 { id: '2', title: 'Pago' }, { id: '3', title: 'Cancelado' }];
  constructor(private dataService: DataService) { }

  ngOnInit() {
    console.log('foi');
    this.situacaoSelectedOption = this.situacoes[1];
    this.loadRecebimentos(this.situacaoSelectedOption);
  }

  loadRecebimentos(situacao: IOption) {
    this.dataService.getRecebimentos(situacao.title).subscribe((dsResult: Recebimento[]) => this.recebimentos = dsResult,
                                              error => console.log(error));
  }

  newCustomer() {
    //this.modalService
    //.open(new CustomerRegisterPageModal())
    //.onApprove(item => this.clientes.push(<Cliente>item))
    //.onDeny(item => alert('Cadastro Cancelado!'))
    //;
  }

  alterCustomer(indice: number) {
      //this.modalService
      //.open(new CustomerRegisterPageModal(this.clientes[indice]))
      //.onApprove(item => this.clientes[indice] = <Cliente>item)
      //.onDeny(item => alert('Alteração Cancelada!'))
     // ;
  }

}
