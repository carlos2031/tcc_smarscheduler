


export class Recebimento {
    public CodigoDoAgendamento: number;
    public Cliente: string;
    public Id: string;
    public Vencimento: Date;
    public Valor: number;
    public Situacao: string;
    public SituacaoId: number;
}
