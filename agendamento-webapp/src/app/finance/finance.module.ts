import { NgModule, LOCALE_ID } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import localePt from '@angular/common/locales/pt';

import { BillsToReceiveComponent } from './pages/bills-to-receive/bills-to-receive.component';
import { SharedModule } from './../shared/shared.module';
import { FinanceRoutingModule, routedComponents } from './finance.routing';
import { StatusRecebimentoPipe } from './pipes/status-receipt.pipe';
import { FilterRowPipe } from './pipes/filter-row.pipe';

registerLocaleData(localePt);

@NgModule({
    imports: [
        SharedModule,
        FinanceRoutingModule
    ],
    exports: [],
    declarations: [
        BillsToReceiveComponent,
        StatusRecebimentoPipe,
        FilterRowPipe
    ],
    providers: [{ provide: LOCALE_ID, useValue: 'pt-BR' } ],
})
export class FinanceModule { }
