import { NgModule, Component } from '@angular/core';
import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CustomerPageComponent } from './pages/customer-page/customer-page.component';

const appRoutes: Routes = [
    { path: '', component: CustomerPageComponent },
];

@NgModule({
    imports: [RouterModule.forChild(appRoutes)],
    exports: [RouterModule],
    declarations: []
})
export class CustomerRountingModule {

}
