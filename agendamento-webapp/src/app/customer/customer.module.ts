import { CoreModule } from './../core/core.module';
import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { CustomerPageComponent } from 'app/customer/pages/customer-page/customer-page.component';
import { CustomerRegisterPageComponent } from 'app/customer/pages/customer-register-page/customer-register-page.component';
import { FilterRowPipe } from 'app/customer/pipes/filter-row.pipe';
import { CustomerRountingModule } from './customer.rounting';

@NgModule({
    imports: [
        SharedModule,
        //AppRountingModule,
        CustomerRountingModule
    ],
    exports: [],
    declarations: [
        CustomerPageComponent,
        CustomerRegisterPageComponent,
        FilterRowPipe
    ],
    providers: [],
    entryComponents: [CustomerRegisterPageComponent]
})
export class CustomerModule {


}
