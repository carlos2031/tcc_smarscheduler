import { ComponentModalConfig, ModalSize } from 'ng2-semantic-ui';
import { CustomerRegisterPageComponent } from 'app/customer/pages/customer-register-page/customer-register-page.component';

export class CustomerRegisterPageModal extends ComponentModalConfig<IConfirmModalContext, void, void> {
    constructor(objeto: any = null, title: string = '', question: string = '', size = ModalSize.Small) {
        super(CustomerRegisterPageComponent, { title, question, objeto });

        this.isClosable = true;
        this.transitionDuration = 200;
        this.size = size;
    }
}
