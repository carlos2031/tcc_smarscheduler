

export class Cliente
{
    public Id: string;
    public Nome: string;
    public CnpjCpf: string;
    public Contato: string;
    public Email: string;
    public Situacao: string;
    public SituacaoId: string;
    public DataNascimento: Date;
    public Observacao: string;
    
}