import { Component, OnInit } from '@angular/core';
import { SuiModal } from 'ng2-semantic-ui';
import { Cliente } from 'app/customer/models/Cliente';

@Component({
  selector: 'app-customer-register-page',
  templateUrl: './customer-register-page.component.html',
  styleUrls: ['./customer-register-page.component.css']
})
export class CustomerRegisterPageComponent implements OnInit {
  public situacoes: IOption[] = [{ id: '1', title: 'Ativo' }, { id: '2', title: 'Inativo' }];
  public situacaoSelected: IOption;
  public datePickerMode: string;
  public datePicker: Date;
  public datePickerLocal: string;
  customer: Cliente;

  constructor(public modal: SuiModal<IConfirmModalContext, void, void>) { }

  ngOnInit() {
    console.log(this.modal.context.objeto);
    this.loadCustomer(this.modal.context.objeto);
    this.loadConfig();
  }

  loadConfig() {
    this.datePickerMode = 'date';
    this.datePickerLocal = 'bottom';
    this.customer.DataNascimento = new Date();
  }

  loadSituacao(opcao: IOption) {
    this.customer.Situacao = opcao.title;
    this.customer.SituacaoId = opcao.id;
  }

  loadCustomer(objCliente: any) {
    this.customer = (objCliente == null) ? new Cliente() : <Cliente>objCliente;
    this.situacaoSelected = this.situacoes.filter(item => item.title === this.customer.Situacao)[0];
    // console.log(objCliente);
    // console.log(this.customer);
  }


}
