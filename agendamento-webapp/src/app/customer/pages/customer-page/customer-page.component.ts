import { Component, OnInit } from '@angular/core';
import { SuiModalService } from 'ng2-semantic-ui';
import { Cliente } from 'app/customer/models/Cliente';
import { CustomerRegisterPageModal } from 'app/customer/models/CustomerRegisterPageModal';
import { DataService } from 'app/core';

@Component({
  selector: 'app-customer-page',
  templateUrl: './customer-page.component.html',
  styleUrls: ['./customer-page.component.css'],
})
export class CustomerPageComponent implements OnInit {
  public clientes: Cliente[];
  public searchString: string;
  public situacaoSelectedOption: IOption;
  public situacoes: IOption[] = [{ id: '0', title: 'Todos' }, { id: '1', title: 'Ativos' }, { id: '2', title: 'Inativos' }];
  constructor(public dataService: DataService,
              public modalService: SuiModalService) { }

  ngOnInit() {
    this.situacaoSelectedOption = this.situacoes[1];
    this.loadClientes(this.situacaoSelectedOption);
  }

  loadClientes(situacao: IOption) {
    this.dataService.getClientes(situacao.title).subscribe((dsResult: Cliente[]) => this.clientes = dsResult,
                                              error => console.log(error));
  }

  newCustomer() {
    this.modalService
    .open(new CustomerRegisterPageModal())
    .onApprove(item => this.clientes.push(<Cliente>item))
    .onDeny(item => console.log('Cadastro Cancelado!'))
    ;
  }

  alterCustomer(indice: number) {
      this.modalService
      .open(new CustomerRegisterPageModal(this.clientes[indice]))
      .onApprove(item => this.clientes[indice] = <Cliente>item)
      .onDeny(item => console.log('Alteração Cancelada!'))
      ;
  }
}
