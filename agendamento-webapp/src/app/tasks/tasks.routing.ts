import { TaskboardComponent } from './pages/taskboard/taskboard.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TasksListComponent } from './pages/tasks-list/tasks-list.component';
import { TasksComponent } from './tasks.component';

const routes: Routes = [
    {
        path: '',
        component: TasksComponent,
        children: [
            {
                path: '',
                component: TasksListComponent
            }
            ,
            {
                path: ':id',
                component: TaskboardComponent,
            }
        ]
    }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class TasksRoutingModule { }

export const routedComponents = [];
