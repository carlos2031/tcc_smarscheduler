import { ITaskItem } from './../../domain/ITaskItem';
import { ITaskList } from './../../domain/ITaskList';
import { Component, OnInit, Input } from '@angular/core';
import { TaskItem } from '../../domain/TaskItem';
import { DragulaService } from 'ng2-dragula';

@Component({
  selector: 'app-taskboard-list',
  templateUrl: './taskboard-list.component.html',
  styleUrls: ['./taskboard-list.component.css']
})
export class TaskboardListComponent implements OnInit {
  @Input() list: ITaskList;
  displayAddCard = false;
 
  constructor(private dragulaService:DragulaService) {
    dragulaService.over.subscribe((value) => {
      this.updateTaskStatus(value.slice(1));
    });
   }

  toggleDisplayAddCard() {
    this.displayAddCard = ! this.displayAddCard;
  }

  ngOnInit() {
  }

  updateTaskStatus(args)
  {
    let [e, el, container] = args;
    console.log(container);
  }

}
