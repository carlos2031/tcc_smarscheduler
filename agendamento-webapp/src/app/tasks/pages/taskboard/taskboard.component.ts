import { TaskboardManager } from './../../domain/TaskboardManager';
import { IOptionList } from './../../domain/IOptionList';
import { ITaskList } from './../../domain/ITaskList';
import { Component } from '@angular/core';
import { DragulaService } from 'ng2-dragula';
import { ITaskItem } from './../../domain/ITaskItem';
import { TaskItem } from '../../domain/TaskItem';
import { DataService } from 'app/core';

@Component({
  selector: 'app-taskboard',
  templateUrl: './taskboard.component.html',
  styleUrls: ['./taskboard.component.css']
})
export class TaskboardComponent {
  taskboardManager: TaskboardManager;

  public chkItem = false;
  public badCurly = '';
  constructor(private dragulaService:DragulaService,
              public dataService: DataService) {
  }

  ngOnInit()
  {
    this.setMockData();
  }

  public iconClick()
  {
      if(this.chkItem)
      {
        this.badCurly = 'check circle link icon green';
        this.chkItem = false;
      }
      else
      {
        this.badCurly = 'circle outline link icon grey';
        this.chkItem = true;
      }
  }

  setMockData(): void {
    const optionList = this.dataService.getTaskboardOptionList()
                           .subscribe((dsResult: IOptionList[]) => this.taskboardManager = new TaskboardManager(dsResult),
                           error => console.log(error));
    const tasks = this.dataService.getTaskboardTasks('')
                           .subscribe((dsResult: ITaskItem[]) => this.taskboardManager.addCard(dsResult),
                           error => console.log(error));
  }

  addItem(){
  }
}
