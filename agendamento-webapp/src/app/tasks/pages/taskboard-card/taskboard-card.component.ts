
import { Component, OnInit, Input } from '@angular/core';
import { ITaskItem } from '../../domain/ITaskItem';

@Component({
  selector: 'app-taskboard-card',
  templateUrl: './taskboard-card.component.html',
  styleUrls: ['./taskboard-card.component.css']
})
export class TaskboardCardComponent implements OnInit {

  @Input() card: ITaskItem;
  constructor() { }
 
  ngOnInit() {
    //console.log(this.card);
  }
 
  dragStart(ev) {
    ev.dataTransfer.setData('text', ev.target.id);
  }
}
