import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { DragulaModule } from 'ng2-dragula/ng2-dragula';

import { TasksComponent } from './tasks.component';
import { TasksRoutingModule } from './tasks.routing';
import { TasksListComponent } from './pages/tasks-list/tasks-list.component';
import { TasksDetailsComponent } from './pages/tasks-details/tasks-details.component';
import { TaskboardComponent } from './pages/taskboard/taskboard.component';
import { TaskboardListComponent } from './pages/taskboard-list/taskboard-list.component';
import { TaskboardCardComponent } from './pages/taskboard-card/taskboard-card.component';

@NgModule({
    imports: [
        SharedModule,
        TasksRoutingModule,
        DragulaModule
    ],
    exports: [],
    declarations: [TasksComponent, TasksListComponent, TasksDetailsComponent, TaskboardComponent, TaskboardListComponent, TaskboardCardComponent],
    providers: [],
})
export class TasksModule { }
