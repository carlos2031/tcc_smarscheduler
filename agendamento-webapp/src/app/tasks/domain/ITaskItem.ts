export interface ITaskItem {
    Id: string;
    Titulo: string;
    SubTitulo: string;
    SituacaoId: string;
    AtividadesFilhas?: ITaskItem[];
  }

  // {
  //   "Id": "8cff5sda4-5f4f-418f-bb8b-d6c2e0b4bb0c",
  //   "Titulo": "Atividade A",
  //   "SubTitulo": "subtitulo da atividade A",
  //   "AtividadesFilhas": [{
  //     "Id": "5aa0da948d080433cc5bf1c1",
  //     "Titulo": "Teste",
  //     "SubTitulo": "Impressão de xml",
  //     "SituacaoId": "5aa0d8a68d080433cc5bf1a6",
  //     "Situacao": {
  //       "Id": "5aa0d8a68d080433cc5bf1a6",
  //       "Titulo": "Em Andamento"
  //     }
  //   }],
  //   "SituacaoId": "5aa0d8b68d080433cc5bf1a7",
  //   "Situacao": {
  //     "Id": "5aa0d8a68d080433cc5bf1a6",
  //     "Titulo": "Em Andamento"
  //   }
  // }