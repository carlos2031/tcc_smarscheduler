import { IOptionList } from './IOptionList';
import { ITaskList } from './ITaskList';
import { ITaskItem } from './ITaskItem';

export class TaskboardManager {
    public TaskList: ITaskList[];

    constructor(optionList: IOptionList[]) {
        this.TaskList = new Array<ITaskList>();
        optionList.forEach(element => {
            this.TaskList.push({Option: element, Cards: []});
        });
    }

    addCard(taskItens: ITaskItem[]) {
        this.TaskList.forEach(element => {
            const eId: string =  element.Option.Id;
            element.Cards = taskItens.filter((task: ITaskItem)  => task.SituacaoId === eId);
        });
    }

    getTaskOptionList()
    {
        return this.TaskList;
    }
}
