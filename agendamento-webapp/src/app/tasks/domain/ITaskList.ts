import { ITaskItem } from './ITaskItem';
import { IOptionList } from './IOptionList';

export interface ITaskList {
    Option: IOptionList;
    Cards: ITaskItem[];
}





