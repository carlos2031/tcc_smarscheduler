import { ITaskItem } from './ITaskItem';

export class TaskItem {
  cards: Object = {};
  lastid = -1;
  
  addCard(card: ITaskItem) {
    card.Id = String(++this.lastid);
    this.cards[card.Id] = card;
    return (card.Id);
  }

  getCard(cardId: string) {
    return this.cards[cardId];
  }
 
  newCard(description: string): string {
   const card = {} as ITaskItem;
   card.Titulo = description;
   return (this.addCard(card));
  }
}
