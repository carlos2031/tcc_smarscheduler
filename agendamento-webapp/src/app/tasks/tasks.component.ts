import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-process-page',
    templateUrl: './tasks.component.html',
    styleUrls: ['./tasks.component.css']
})

export class TasksComponent implements OnInit {
    public situacaoSelectedOption: IOption;
    public situacoes: IOption[] = [{ id: '0', title: 'Todos' }, { id: '1', title: 'Em andamento' }, { id: '2', title: 'Finalizadas' },{ id: '3', title: 'Canceladas' }];
  
    constructor() { }

    ngOnInit() { }
}