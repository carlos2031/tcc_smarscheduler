import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-headbar',
  templateUrl: './headbar.component.html',
  styleUrls: ['./headbar.component.css']
})
export class HeadBarComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  public addCustomer()
  {
    this.router.navigateByUrl('/scheduling')
  }
}
