export class SearchItem {
  public name: string;
  public artist: string;
  public link: string;
  public thumbnail: string;
  public artistId: string;

  constructor(nameOld: string, artistOld :string, linkOld: string,
              thumbnailOld: string, artistIdOld: string) {
    this.name = nameOld;
    this.artist = artistIdOld;
    this.link = linkOld;
    this.thumbnail = thumbnailOld;
    this.artist = artistIdOld;
  }
}
