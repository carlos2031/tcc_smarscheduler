import { IOptionList } from './../../tasks/domain/IOptionList';
import { ITaskList } from './../../tasks/domain/ITaskList';
import { ITaskItem } from './../../tasks/domain/ITaskItem';
import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { 
         TiposDeAgendamento, 
         TiposDeNotificacao, 
         SituacoesDeAgendamento, 
         DuracoesPadraoDeServico 
} from 'app/scheduling/services/constants';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/of';
// import 'rxjs/add/operator/delay';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/do';
import { Cliente } from 'app/customer/models/Cliente';
import { HorariosDisponiveis } from 'app/scheduling/models/HorariosDisponiveis';


@Injectable()
export class DataService {

    readonly apiItunesRoot = 'https://itunes.apple.com/search';
    readonly UrlApi = 'http://localhost:3000';
    readonly UrlApiAgendamento = `${this.UrlApi}/agenda`;
    readonly UrlApiEmpresas = `${this.UrlApi}/empresas`;
    readonly UrlApiProfissionais = `${this.UrlApi}/profissionais`;
    readonly UrlApiServicos = `${this.UrlApi}/servicos`;
    readonly UrlApiHorariosDisponiveis = `${this.UrlApi}/horariosDisponiveis`;
    readonly UrlApiClientes = `${this.UrlApi}/clientes`;
    readonly UrlApiAuth = `${this.UrlApi}/api`;
    readonly UrlTaskboardItens = `${this.UrlApi}/atividades`;
    readonly UrlTaskboardOptionList = `${this.UrlApi}/situacaoDeAtividades`;
    
    
    constructor(private http: Http) {}

    getAgendamentoTiposDeCadastro(): Observable<any> {
        return Observable.of(TiposDeAgendamento); //.delay(100);
        // return this.http.get(<any>TiposDeAgendamento)
        //     .map((response: Response) => {
        //         console.log("mock data" + response.json());
        //         return response.json();
        //     }
        // );
    } 

    getAgendamentoTiposDeNotificacao(): Observable<any> {
        return Observable.of(TiposDeNotificacao);
        // return this.http.get(<any>TiposDeNotificacao)
        //     .map((response: Response) => {
        //         console.log("mock data" + response.json());
        //         return response.json();
        //     }
        // );
    } 

    getAgendamentoSituacoes(): Observable<any> {
        return Observable.of(SituacoesDeAgendamento);
        // return this.http.get(<any>SituacoesDeAgendamento)
        //     .map((response: Response) => {
        //         console.log("mock data" + response.json());
        //         return response.json();
        //     }
        // );
    } 

    getServicoDuracoesPadrao(): Observable<any> {
        return Observable.of(DuracoesPadraoDeServico);
        // return this.http.get(<any>DuracoesPadraoDeServico)
        //     .map((response: Response) => {
        //         console.log("mock data" + response.json());
        //         return response.json();
        //     }
        // );
    } 

    getProfissionais(): Observable<IOption[]> {
        return this.http.get(this.UrlApiProfissionais)
            .map((response: Response) => {
                let result = response.json().map(item => { 
                    return {
                        id:item.Id,
                        title:item.Nome
                    };
                });
                //console.log(result);
                return result;
            }
        );
    } 

    getServicos(query: string): Observable<IOption[]> {
        return this.http.get(this.UrlApiServicos)
            .map((response: Response) => {
                let result = response.json().map(item => { 
                    return {
                        id:item.Id,
                        title:item.Nome
                    };
                });
                //console.log(result);
                return result;
            }
        );
    } 

    getHorariosPorProfissionalEData(idProfissional: string, data:Date): Observable<IOption[]> {
        let actionHorarios = this.UrlApiHorariosDisponiveis + `?profissionalId=${idProfissional}`
        //actionHorarios += `&data=${data.getDate()}`;
        //console.log(actionHorarios);
        return this.http.get(actionHorarios)
            .map(this.extractTimesFromData);
    } 

    private extractTimesFromData(res)
    {
        //(response: Response) => {
            let horarioDisponiveis = res.json().map(item => <HorariosDisponiveis>item);
            
            const horarios = horarioDisponiveis[0].Horarios;
            if(horarios.length <= 0) return [];

            let result = horarios.map(horario => {
                return {
                    id: horario.replace(':',''),
                    title: horario
                }
            });
           
            //console.log(result);
            return result;
        //}
    }

    getClientes(situacao: string): Observable<Cliente[]> {

        let UrlClientes = this.UrlApiClientes;
        if(situacao != '' && situacao != 'Todos')
        {
            UrlClientes += `?Situacao=${situacao.substring(0,situacao.length-1)}`
        }
        console.log(UrlClientes);
        return this.http.get(UrlClientes)
            .map((response: Response) => {
                let result = response.json().map(item => <Cliente>item);
                //console.log(result);
                return result;
            }
        );
    } 

    register(data: any){
        return this.http.post(this.UrlApiAuth + '/v1/account/register', data)
        .map((res: Response) => res.json());
    }

    authenticate(data: any) {
        var dataType = "grant_type=password&username=" + data.email + "&password=" + data.password;
        let headers = new Headers({ "Content-Type": "application/x-www-form-urlencoded" });
        let options = new RequestOptions({ headers: headers });
        return this.http.post(this.UrlApiAuth + '/v1/account/authenticate', dataType, options)
        .map((res: Response) => res.json());
    }

    getAgendaProfissionais(): Observable<any> {
        // get users from api
        return this.http.get(this.UrlApiAgendamento)//, options)
            .map((response: Response) => {
                //console.log("mock data" + response.json());
                return response.json();
            }
        );
    } 

    getEmpresas(): Observable<any> {
        // get users from api
        return this.http.get(this.UrlApiEmpresas)//, options)
            .map((response: Response) => {
                //console.log("mock data" + response.json());
                return response.json();
            }
        );
    } 

    getItunesMusic(term: string): Observable<IOption[]> {
        return this.http.get(`${this.apiItunesRoot}?term=${term}&media=music&limit=20`)
                        .map(this.extractOptionsData)
                        //.do(data => console.log('All: ' + JSON.stringify(data)))
                        .catch(this.handleError);
    }
    
    extractOptionsData(res) {
        //let body = res.json();
        //console.log(body);
        let result = res.json().results.map(item => { 
          return {
              id:item.artistId,
              title:item.trackName
          };
        });
        //console.log(result);
        return result;
    } 
    
    handleError(error: Response) {
        console.error(error);
        return Observable.throw(error.json() || 'Server error');
    }

    getTaskboardTasks(situacao: string): Observable<ITaskItem[]> {
        return this.http.get(this.UrlTaskboardItens)
            .map((response: Response) => {
                let result = response.json().map(item => <ITaskItem>item);
                //console.log(result);
                return result;
            }
        );
    } 

    getTaskboardOptionList(): Observable<IOptionList[]> {
        return this.http.get(this.UrlTaskboardOptionList)
            .map((response: Response) => {
                let result = response.json().map(item => <IOptionList>item);
                //console.log(result);
                return result;
            }
        );
    } 
}