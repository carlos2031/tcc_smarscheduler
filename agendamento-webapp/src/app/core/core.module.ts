import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRountingModule } from '../app.rounting.module';

import { SharedModule } from './../shared/shared.module';
import { FooterComponent } from './footer/footer.component';
import { HeadBarComponent } from './headbar/headbar.component';
import { SubMenuComponent } from './sub-menu/sub-menu.component';
import { DataService } from 'app/core/services/data.services';
import { SuiDropdownModule } from 'ng2-semantic-ui/dist';
import { SuiPopupModule } from 'ng2-semantic-ui';
import { SpinnerComponent } from './spinner/spinner.component';

@NgModule({
    imports: [
        CommonModule,
        SuiDropdownModule,
        SuiPopupModule,
        AppRountingModule
    ],
    declarations: [
        FooterComponent,
        HeadBarComponent,
        SubMenuComponent,
        SpinnerComponent
    ],
    exports: [
        FooterComponent,
        HeadBarComponent,
        SubMenuComponent,
        SpinnerComponent
    ],
    providers: [
        DataService
    ],
})
export class CoreModule { }
